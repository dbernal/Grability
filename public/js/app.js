//Stard document ready
$( document ).ready(function() {
    $("#btnAceptar").click(function(){		
		generateOperations();

    });
});

//the number of test cases for iterating
function generateOperations(){
	//number of test cases
	var iteration = $("#txtTestCase").val();
	
	for (var i = 0; i < iteration; i++) {
		//gerenete template
		var dimensions = getDimensions(i);
		$("#divTestCase").append(dimensions);
	};
	$(".btnGenerar").on("click", function(){
    	generteSubOperations(this);
    });
}

//Generate subOperations for the test cases
function generteSubOperations(component) {
	//get Iteration number
	var iteration = $(component).parent().find("input").val();
	var tBody = "";
	//Generate template
	for (var i = 0; i < parseInt(iteration); i++) {
		tBody += getBody(i);		
	}  	
	var template = "<fieldset>" +
				     "<legend><b>Test Case</b></legend>" + 
				     	tBody + 
				   "</fieldset>";

	$("#divTestCase").append(template);		
}

//Create array to send with the data to calculate 
function sendData(testCaseList){
	var array = new Array();
	//Iteration test cases
	for (var i = testCaseList.length - 1; i >= 0; i--) {		
		var operations = new Array();
		//Iteration suboperations
		for (var j = 0; j < testCaseList[i].subOperations.length; j++) {
			var item = {
				"operationValue" : operationValue,
				"operationType" : operationType
		    }

		    operations.push(item);
	    };		
		
		var operation = {
			"sizeArray" : sizeArray,
			"operations" : operations
		}

		array.push(operation);
	};
	//call function with array to send
	sendOperations(array);	    
}

//call web service with ajax
function sendOperations(operationList) {
    $.ajax( {
        type : "POST",
        url : "http://localhost/app/rest",
        async : false,
        data :  { "data" : operationList},
        cache : false,
        dataType : 'json',
        success : function (data) {
            for (var i = 0; i < data.resultList.length; i++) {
            	alert(data.resultList[i]);
            };
        },
        error : function () {
            alert("Functional Error")
        }
    });
  }